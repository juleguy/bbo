from os.path import exists

from evaluation import compute_ecdf
from postprocessing import extract_BBO_experiment_data
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

ECDF_TARGETS = np.arange(-10, -1, 0.01)

one_times_dotproduct_optim_medium_pop_old_alg = ["18_BBO_1xDotProduct_hyperoptim"] + ["18_BBO_1xDotProduct_hyperoptim_"+str(i) for i in range(2, 11)]
one_times_dotproduct_optim_small_pop = ["21_BBO_1xDotProduct_hyperoptim_small_pop_"+str(i) for i in range(1, 11)]
one_times_dotproduct_optim_medium_pop_new_alg = ["22_BBO_1xDotProduct_hyperoptim_medium_pop_new_alg_"+str(i) for i in range(1, 11)]

def extract_calls_obj_values_lists(paths):
    calls_lists = []
    obj_values_lists = []

    for path in paths:

        if exists(path):
            data = extract_BBO_experiment_data(path)
            calls_lists.append(data["dataset_success_n_calls"])
            obj_values_lists.append(data["dataset_success_obj_value"])

    return calls_lists, obj_values_lists


def plot(paths_list, names):
    colors = sns.color_palette()

    plt.figure(figsize=(15, 10))
    plt.xlim(0, 310)
    plt.ylim(0, 1)

    for i, paths in enumerate(paths_list):
        calls_lists, obj_values_lists = extract_calls_obj_values_lists(paths)

        for j in range(len(calls_lists)):
            calls, ecdf_vect = compute_ecdf([calls_lists[j]], [obj_values_lists[j]], targets=ECDF_TARGETS)
            if i == 0:
                sns.lineplot(calls, ecdf_vect, dashes=[(2, 2)], style=True, color=colors[i])

        calls, ecdf_vect = compute_ecdf(calls_lists, obj_values_lists, targets=ECDF_TARGETS)

        p = sns.lineplot(calls, ecdf_vect, label=names[i], color=colors[i])
    # p.legend_.remove()
    plt.show()


plot(
    paths_list=[one_times_dotproduct_optim_small_pop, one_times_dotproduct_optim_medium_pop_new_alg, one_times_dotproduct_optim_medium_pop_old_alg],
    names=["Small pop.", "Medium pop. new alg", "Medium pop. old alg"]
)
