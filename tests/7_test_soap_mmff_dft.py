import json
import os

from descriptor import SOAPDesc
from geometry import dft_cache_xyz

path_json_cache = "/home/jleguy/Documents/these/prod/data/00_datasets/DFT/cache_OD9_step7.json"

smi_list = []

with open(path_json_cache, "r") as f:
    data = json.load(f)
    for k in data.keys():
        smi_list.append(k)


soap_mmff_builder = SOAPDesc(cache_location=os.environ["CACHE_CHEM2020"])
soap_dft_builder = SOAPDesc(cache_location=os.environ["CACHE_CHEM2020"], geometry_function=dft_cache_xyz,
                            geometry_function_parameters={})

soap_mmff, success = soap_mmff_builder.transform(smi_list[:10])
soap_dft, success = soap_dft_builder.transform(smi_list[:10])

print(soap_mmff)

print(soap_dft)