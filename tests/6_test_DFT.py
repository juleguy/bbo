import json

from objective_dft import DFTEnergyObjective

path_json_cache = "/home/jleguy/Documents/these/prod/data/00_datasets/DFT/cache_OD9_step7.json"


smi_list = []

with open(path_json_cache, "r") as f:
    data = json.load(f)
    for k in data.keys():
        smi_list.append(k)

obj = DFTEnergyObjective("homo", "tests/6_dft/dft_files", "/home/jleguy/Documents/these/prod/data/00_datasets/DFT/cache_OD9_step7.json", 1)

y = obj.transform(smi_list[:10])

print(y)