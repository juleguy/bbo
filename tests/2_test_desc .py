import os
import time

from cache import Cache
from descriptor import CoulombMatrixDesc, SOAPDesc, MBTRDesc, ShinglesVectDesc
from rdkit.Chem.rdmolfiles import MolFromSmiles, MolToSmiles
import numpy as np

cache = Cache()

cm_builder = CoulombMatrixDesc(cache=cache, working_dir="working_dir", n_atoms_max=100, n_jobs=12)

desc, success = cm_builder.transform(
    [MolToSmiles(MolFromSmiles("CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F")),
     MolToSmiles(MolFromSmiles("O=C1NC(=O)SC1Cc4ccc(OCC3(Oc2c(c(c(O)c(c2CC3)C)C)C)C)cc4"))])

print(desc)
print(success)
print(cache.smi_dict)

desc, success = cm_builder.transform(
    [MolToSmiles(MolFromSmiles("CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F")),
     MolToSmiles(MolFromSmiles("O=C1NC(=O)SC1Cc4ccc(OCC3(Oc2c(c(c(O)c(c2CC3)C)C)C)C)cc4")),
     MolToSmiles(MolFromSmiles("O=S(=O)(N(C)C)c2cc1C(\c3c(Sc1cc2)cccc3)=C/CCN4CCN(C)CC4")),
     MolToSmiles(MolFromSmiles("O=C=O"))])

print(desc)
print(success)
print(cache.smi_dict)

desc, success = cm_builder.transform(
    [MolToSmiles(MolFromSmiles("CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F")),
     MolToSmiles(MolFromSmiles("O=C1NC(=O)SC1Cc4ccc(OCC3(Oc2c(c(c(O)c(c2CC3)C)C)C)C)cc4")),
     MolToSmiles(MolFromSmiles("O=S(=O)(N(C)C)c2cc1C(\c3c(Sc1cc2)cccc3)=C/CCN4CCN(C)CC4")),
     MolToSmiles(MolFromSmiles("O=C=O"))])

print(desc)
print(success)
print(cache.smi_dict)

# Reading dataset
dataset_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_all.smiles"
init_pop_smi = []
with open(dataset_path, "r") as f:
    for smi in f.readlines()[:2000]:
        init_pop_smi.append(MolToSmiles(MolFromSmiles(smi.rstrip())))

tstart = time.time()

desc, success = cm_builder.transform(init_pop_smi)

print(time.time() - tstart)

print(desc)
print(success)

init_pop_smi_errors = np.array(init_pop_smi)[success == False]
print(len(init_pop_smi_errors))

desc, success = cm_builder.transform(init_pop_smi_errors)
print(desc)
print(success)

print("SOAP")

soap_builder = SOAPDesc(cache=cache, n_jobs=12, working_dir="working_dir")

tstart = time.time()


desc, success = soap_builder.transform(init_pop_smi)
print(desc)
print(success)

print(time.time() - tstart)


print("SOAP Again")

tstart = time.time()

desc, success = soap_builder.transform(init_pop_smi)
print(desc)
print(success)

print(time.time() - tstart)


print("SOAP from nothing")

soap_builder = SOAPDesc(cache=Cache(), n_jobs=12, working_dir="working_dir")


tstart = time.time()

desc, success = soap_builder.transform(init_pop_smi)
print(desc)
print(success)

print(time.time() - tstart)


print("MBTR")

mbtr_builder = MBTRDesc(cache=cache, n_jobs=12, working_dir="working_dir", species="default")

tstart = time.time()


desc, success = mbtr_builder.transform(init_pop_smi)
print(desc.shape)
print(desc)
print(success)

print(time.time() - tstart)


print("MBTR Again")

tstart = time.time()

desc, success = mbtr_builder.transform(init_pop_smi)
print(desc)
print(success)

print(time.time() - tstart)


print("MBTR from nothing")

mbtr_builder = MBTRDesc(cache=Cache(), n_jobs=12, working_dir="working_dir")


tstart = time.time()

desc, success = mbtr_builder.transform(init_pop_smi)
print(desc)
print(success)

print(time.time() - tstart)


print("SHG_1")

shg_builder = ShinglesVectDesc(cache)

tstart = time.time()

desc, success = shg_builder.transform(init_pop_smi)
print(desc.shape)
print(desc)
print(success)

print(time.time() - tstart)


print("SHG Again")

tstart = time.time()

desc, success = shg_builder.transform(init_pop_smi)
print(desc.shape)
print(desc)
print(success)

print(time.time() - tstart)
