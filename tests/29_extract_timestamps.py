from postprocessing import extract_time_best_BBO, extract_time_best_EvoMol

homogeneous_timestamps, best_score_homogeneous_timestamps = extract_time_best_BBO("/home/jleguy/Documents/these/prod/data/07_BBO/03_bbo_optim/paper/02.01_optim_HOMO_shg_count_dp_10_steps/1")

print(homogeneous_timestamps[:200])
print(best_score_homogeneous_timestamps[:200])

homogeneous_timestamps, best_score_homogeneous_timestamps = extract_time_best_EvoMol("/home/jleguy/Documents/these/prod/data/07_BBO/03_bbo_optim/paper/01.01_EvoMol_from_methane_optim_HOMO_rdkit_nocache/1")
print(homogeneous_timestamps[:300])
print(best_score_homogeneous_timestamps[:300])