from evaluation import compute_ecdf
from postprocessing import extract_BBO_experiment_data
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

ECDF_TARGETS = np.arange(-10, -1, 0.01)

buggy_norm_path = "BBO_buggy_Normalization"
buggy_norm_path_2 = "BBO_buggy_Normalization_2"
buggy_norm_path_3 = "BBO_buggy_Normalization_3"
no_norm_path = "13_BBO_no_normalization"
no_norm_path_2 = "13_BBO_no_normalization_2"
no_norm_path_3 = "13_BBO_no_normalization_3"

buggy_norm_data = extract_BBO_experiment_data(buggy_norm_path)
buggy_norm_data_2 = extract_BBO_experiment_data(buggy_norm_path_2)
buggy_norm_data_3 = extract_BBO_experiment_data(buggy_norm_path_3)

no_norm_data = extract_BBO_experiment_data(no_norm_path)
no_norm_data_2 = extract_BBO_experiment_data(no_norm_path_2)
no_norm_data_3 = extract_BBO_experiment_data(no_norm_path_3)

buggy_norm_obj_calls, buggy_norm_ecdf_vect = compute_ecdf([buggy_norm_data["dataset_success_n_calls"]],
                                                          [buggy_norm_data["dataset_success_obj_value"]],
                                                          ECDF_TARGETS)

buggy_norm_obj_calls_2, buggy_norm_ecdf_vect_2 = compute_ecdf([buggy_norm_data_2["dataset_success_n_calls"]],
                                                              [buggy_norm_data_2["dataset_success_obj_value"]],
                                                              ECDF_TARGETS)

buggy_norm_obj_calls_3, buggy_norm_ecdf_vect_3 = compute_ecdf([buggy_norm_data_3["dataset_success_n_calls"]],
                                                              [buggy_norm_data_3["dataset_success_obj_value"]],
                                                              ECDF_TARGETS)

no_norm_obj_calls, no_norm_ecdf_vect = compute_ecdf([no_norm_data["dataset_success_n_calls"]],
                                                    [no_norm_data["dataset_success_obj_value"]],
                                                    ECDF_TARGETS)

no_norm_obj_calls_2, no_norm_ecdf_vect_2 = compute_ecdf([no_norm_data_2["dataset_success_n_calls"]],
                                                        [no_norm_data_2["dataset_success_obj_value"]],
                                                        ECDF_TARGETS)

no_norm_obj_calls_3, no_norm_ecdf_vect_3 = compute_ecdf([no_norm_data_3["dataset_success_n_calls"]],
                                                        [no_norm_data_3["dataset_success_obj_value"]],
                                                        ECDF_TARGETS)

plt.figure(figsize=(15, 10))
plt.xlim(0, 210)
plt.ylim(0, 1)

sns.lineplot(buggy_norm_obj_calls, buggy_norm_ecdf_vect, label="Buggy normalization", color="blue", dashes=[(2, 2)],
             style=True, alpha=0.5)
sns.lineplot(buggy_norm_obj_calls_2, buggy_norm_ecdf_vect_2, label="Buggy normalization", color="blue",
             dashes=[(2, 2)],
             style=True, alpha=0.5)

sns.lineplot(buggy_norm_obj_calls_3, buggy_norm_ecdf_vect_3, label="Buggy normalization", color="blue",
             dashes=[(2, 2)],
             style=True, alpha=0.5)

sns.lineplot(no_norm_obj_calls, no_norm_ecdf_vect, label="No normalization", color="orange", dashes=[(2, 2)],
             style=True, alpha=0.5)
sns.lineplot(no_norm_obj_calls_2, no_norm_ecdf_vect_2, label="No normalization", color="orange", dashes=[(2, 2)],
             style=True, alpha=0.5)
sns.lineplot(no_norm_obj_calls_3, no_norm_ecdf_vect_3, label="No normalization", color="orange", dashes=[(2, 2)],
             style=True, alpha=0.5)

agg_norm_obj_calls, agg_norm_ecdf_vect = compute_ecdf([buggy_norm_data["dataset_success_n_calls"],
                                                       buggy_norm_data_2["dataset_success_n_calls"],
                                                       buggy_norm_data_3["dataset_success_n_calls"]],
                                                      [buggy_norm_data["dataset_success_obj_value"],
                                                       buggy_norm_data_2["dataset_success_obj_value"],
                                                       buggy_norm_data_3["dataset_success_obj_value"]],
                                                      targets=ECDF_TARGETS)

agg_no_norm_obj_calls, agg_no_norm_ecdf_vect = compute_ecdf([no_norm_data["dataset_success_n_calls"],
                                                             no_norm_data_2["dataset_success_n_calls"],
                                                             no_norm_data_3["dataset_success_n_calls"]],
                                                            [no_norm_data["dataset_success_obj_value"],
                                                             no_norm_data_2["dataset_success_obj_value"],
                                                             no_norm_data_3["dataset_success_obj_value"]],
                                                            targets=ECDF_TARGETS)

sns.lineplot(agg_norm_obj_calls, agg_norm_ecdf_vect, label="Buggy normalization", color="blue")
sns.lineplot(agg_no_norm_obj_calls, agg_no_norm_ecdf_vect, label="No normalization", color="orange")

plt.show()
