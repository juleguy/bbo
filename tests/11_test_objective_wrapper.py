from evomol.evomol import OPTEvaluationStrategy, ProductEvaluationStrategy, CLScoreEvaluationStrategy
from objective import EvoMolEvaluationStrategyWrapper

objective = EvoMolEvaluationStrategyWrapper(ProductEvaluationStrategy([
    OPTEvaluationStrategy("homo", MM_program="rdkit"),
    CLScoreEvaluationStrategy()
]), n_jobs=10)

scores, other_scores, successes = objective.fit_transform(
    ["C", "CN", "NC1OC2=C3OC2=C3O1", "C1=CN1c1c[nH]oo[nH]1", "OF"])

print(scores)
print(other_scores)
print(successes)

print(objective.keys())

import os

from bboalg import BBOAlg
from descriptor import ShinglesVectDesc, SOAPDesc, MBTRDesc
from merit import ExpectedImprovementMerit, SurrogateValueMerit
from model import GPRSurrogateModelWrapper
from objective import ECFP4SimilarityObjective, QEDObjective
from objective_dft import DFTEnergyObjective
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, Matern, RBF
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from stop_criterion import KStepsStopCriterion, KObjFunCallsFunctionStopCriterion

json_cache_location = os.environ["DATA"] + "/00_datasets/DFT/cache_OD9_step7.json"
dataset_OD9 = os.environ["DATA"] + "/00_datasets/DFT/OD9_7/OD9_7_smi_datasets/train_test_dataset_3000.smi"

smi_list_OD9 = []
test_smi_OD9 = []

with open(dataset_OD9, "r") as f:
    for i, smi in enumerate(f.readlines()):

        if i < 1000:
            smi_list_OD9.append(smi.rstrip())
        else:
            test_smi_OD9.append(smi.rstrip())

guacamol_path = os.environ["DATA"] + "/00_datasets/07_BBO/GuacaMol/train_test_dataset_3000.smi"

smi_list_guacamol = []
test_smi_list_guacamol = []
with open(guacamol_path, "r") as f:
    complete_smiles = f.readlines()

    for line in complete_smiles[:1000]:
        smi_list_guacamol.append(line.rstrip())

    for line in complete_smiles[1000:2000]:
        test_smi_list_guacamol.append(line.rstrip())

descriptor = ShinglesVectDesc(cache_location=None, count=True)
# descriptor = SOAPDesc(cache_location=None, rcut=4, nmax=4, lmax=4, species=["H", "C", "N", "O", "F"], n_jobs=12)
# descriptor = MBTRDesc(cache_location=None, n_jobs=12, atomic_numbers_n=50, inverse_distances_n=50, cosine_angles_n=50)
print(descriptor.get_row_size())

surrogate = GPRSurrogateModelWrapper(GaussianProcessRegressor(1 * RBF(1.0)))

# objective = ECFP4SimilarityObjective(cache_location=None,
#                                        target_smi="CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F", n_jobs=12)

# objective = QEDObjective(cache_location=None, n_jobs=12)

objective = EvoMolEvaluationStrategyWrapper(
    ProductEvaluationStrategy([OPTEvaluationStrategy("homo", n_jobs=2, cache_files=[json_cache_location],
                                                     working_dir_path="/home/jleguy/dft_comput", MM_program="rdkit"),
                              CLScoreEvaluationStrategy()]),
    n_jobs=10)

pipeline = None

alg = BBOAlg(
    init_dataset_smiles=smi_list_OD9,
    test_dataset_smiles_dict=test_smi_OD9,
    descriptor=descriptor,
    objective=objective,
    merit_function=SurrogateValueMerit(descriptor=descriptor, pipeline=pipeline, surrogate=surrogate),
    surrogate=surrogate,
    pipeline=pipeline,
    stop_criterion=KObjFunCallsFunctionStopCriterion(100),
    evomol_parameters={
        "optimization_parameters": {
            "pop_max_size": 300,
            "max_steps": 50,
            "k_to_replace": 10
        },
        "action_space_parameters": {
            "max_heavy_atoms": 9,
            "atoms": "C,N,O,F"
        }
    },
    evomol_init_pop_size=10,
    n_evomol_runs=10,
    n_best_evomol_retrieved=1,
    evomol_init_pop_strategy="random_weighted",
    results_path="BBO_results",
    n_jobs=10,
    save_surrogate_model=False,
    period_save=1,
    period_compute_test_predictions=1
)

alg.run()
