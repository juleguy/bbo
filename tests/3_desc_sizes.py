import os

from descriptor import CoulombMatrixDesc, SOAPDesc, MBTRDesc, ShinglesVectDesc

mols = ["CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F", "C", "CC(=O)NC1=CC=C(C=C1)O"]


cm_builder = CoulombMatrixDesc(cache_location=os.environ["CACHE_CHEM2020"], n_atoms_max=100, n_jobs=12)
soap_builder = SOAPDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12, average="inner")
soap_builder_complete = SOAPDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12, average="off")
mbtr_builder = MBTRDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12)
shg_builder = ShinglesVectDesc(cache_location=os.environ["CACHE_CHEM2020"])

X_cm, success = cm_builder.transform(mols)
X_soap, success = soap_builder.transform(mols)
X_soap_complete, success = soap_builder_complete.transform(mols)
X_mbtr, success = mbtr_builder.transform(mols)
X_shg, success = shg_builder.transform(mols)

def show(X, s):
    print(s)
    print(X)
    print(X.shape)

show(X_cm, "CM")

show(X_soap, "SOAP")

show(X_soap_complete, "SOAP complete")

show(X_mbtr, "MBTR")

show(X_shg, "SHG_1")
