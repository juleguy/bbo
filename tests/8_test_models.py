import os
import time

from descriptor import ShinglesVectDesc
from model import GPRSurrogateModelWrapper, BaggingSurrogateModel, TanimotoDistanceSurrogateModel, DescDistanceSurrogateModel
from objectives.similarity import ECFP4SimilarityObjective
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split
import numpy as np
from scipy.stats import pearsonr
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

path_json_cache = "/home/jleguy/Documents/these/prod/data/00_datasets/DFT/cache_OD9_step7.json"
guacamol_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_all.smiles"

smi_list = []

# with open(path_json_cache, "r") as f:
#     data = json.load(f)
#     for k in list(data.keys())[:1000]:
#         smi_list.append(k)
#
with open(guacamol_path, "r") as f:
    for line in f.readlines()[:1000]:
        smi_list.append(line.rstrip())

print("n smiles : " + str(len(smi_list)))
print("n unique : " + str(len(np.unique(smi_list))))


desc_builder = ShinglesVectDesc(cache_location=os.environ["CACHE_CHEM2020"], count=True)
# desc_builder = SOAPDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12, rcut=4, nmax=6, lmax=6, species=["C", "H", "O", "N", "F"])
# desc_builder = MBTRDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12,
#                         atomic_numbers_n=50, inverse_distances_n=50, cosine_angles_n=50)

print(desc_builder.get_row_size())

X, success = desc_builder.fit_transform(smi_list)

X = X[success]

obj = ECFP4SimilarityObjective(cache_location=os.environ["CACHE_CHEM2020"],
                               target_smi="CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F")
#
# obj = DFTEnergyObjective("homo", cache_location=os.environ["CACHE_CHEM2020"],
#                          json_cache_location=path_json_cache)

# obj = QEDObjective(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12)

smi_list = np.array(smi_list)[success]

y, success = obj.fit_transform(smi_list)

X, y, smi_list = X[success], y[success], smi_list[success]

pipeline = Pipeline([("scaler", StandardScaler())])

def test_model(model):
    X_train_mask, X_test_mask, y_train_mask, y_test_mask = train_test_split(np.arange(len(X)),
                                                                            np.arange(len(y)), test_size=0.2)

    print(np.array_equal(X_train_mask, y_train_mask))
    print(np.array_equal(X_test_mask, y_test_mask))
    print(np.array_equal(X_train_mask, X_test_mask))

    X_train, X_test, y_train, y_test = X[X_train_mask], X[X_test_mask], y[y_train_mask], y[y_test_mask]

    smiles_train = smi_list[X_train_mask]
    smiles_test = smi_list[X_test_mask]

    # X_train = pipeline.fit_transform(X_train)
    # X_test = pipeline.transform(X_test)

    print("data loaded")
    tstart = time.time()

    model.fit(X_train, y_train, training_smiles=smiles_train)

    print("time fit : " + str(time.time() - tstart))

    y_pred = model.predict(X_test)

    std = model.uncertainty(X_test, smiles_list=smiles_test)

    print("MAE : " + str(mean_absolute_error(y_test, y_pred)))
    abs_error = np.abs(y_test - y_pred)
    print("mean sigma : " + str(np.mean(std)))
    print("min abs_error : " + str(min(abs_error)))
    print("max abs_error : " + str(max(abs_error)))
    print("corr : " + str(pearsonr(abs_error, std)))

    sns.scatterplot(x=abs_error, y=std)
    plt.show()


gpr_model = GaussianProcessRegressor(1 * DotProduct(sigma_0=1))

test_model(GPRSurrogateModelWrapper(gpr_model))

print()

test_model(BaggingSurrogateModel(gpr_model, 10, 10))

test_model(TanimotoDistanceSurrogateModel(cache_location=os.environ["CACHE_CHEM2020"],
                                          model=gpr_model, n_jobs=12))

test_model(DescDistanceSurrogateModel(model=gpr_model))

