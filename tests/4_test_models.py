import os
import time

from cache import Cache
from descriptor import SOAPDesc, ShinglesVectDesc, MBTRDesc
from objectives.similarity import ECFP4SimilarityObjective
from sklearn.gaussian_process import GaussianProcessRegressor
import numpy as np

# Reading dataset
from sklearn.metrics import mean_absolute_error, median_absolute_error, max_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize

dataset_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_all.smiles"
guacamol_smiles = []
with open(dataset_path, "r") as f:
    for smi in f.readlines():
        guacamol_smiles.append(smi.rstrip())

cache = Cache()

desc_builder = SOAPDesc(species=["H", "B", "C", "O", "N", "F", "Si", "P", "S", "Cl", "Se", "Br", "I"], n_jobs=12,
                        cache=cache)

desc_builder = MBTRDesc(n_jobs=12, cache=cache, cosine_angles_n=0)

desc_builder = ShinglesVectDesc(cache=cache)

data_smi = guacamol_smiles[:2000]

# X data

tstart = time.time()

X_desc, success = desc_builder.transform(data_smi)

X_desc = X_desc[success]
data_smi = np.array(data_smi)[success]

X = normalize(X_desc)

print("Time transforming X : " + str(time.time() - tstart))


# y data

tstart = time.time()

objective = ECFP4SimilarityObjective("CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F", cache)
y = objective.fit_transform(data_smi)

print("Time transforming y : " + str(time.time() - tstart))


# Separate train/test
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Model fitting
tstart = time.time()
model = GaussianProcessRegressor()
model.fit(X_train, y_train)
print("Time fitting : " + str(time.time() - tstart))

# Model prediction
tstart = time.time()

y_pred = model.predict(X_test)

print("Time predicting : " + str(time.time() - tstart))


print("MAE : " + str(mean_absolute_error(y_test, y_pred)))
print("MedAE : " + str(median_absolute_error(y_test, y_pred)))
print("Max error : " + str(max_error(y_test, y_pred)))

error = np.abs(y_test - y_pred)

print("max error : " + str(y_test[np.argmax(error)]) + " " + str(y_pred[np.argmax(error)]))
