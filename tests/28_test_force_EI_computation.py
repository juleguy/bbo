import os
from os.path import join

from bboalg import BBOAlg
from descriptor import ShinglesVectDesc
from evomol.evomol import OPTEvaluationStrategy
from merit import ExpectedImprovementMerit
from model import GPRSurrogateModelWrapper
from objective import EvoMolEvaluationStrategyWrapper
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, DotProduct
from sklearn.preprocessing import Normalizer
from stop_criterion import KObjFunCallsFunctionStopCriterion



json_cache_location_list = []

init_dataset_smiles = ["C"]

output_data_path = "28_test_force_EI_computation"

init_pop_zero_EI = True

def run(path):

    # Parallel parameters
    n_jobs_per_model = 1

    # BBO parameters
    save_surrogate_model = False
    period_save = 1
    period_compute_test_predictions = 5
    max_obj_calls = 20

    # EvoMol parameters
    evomol_pop_max_size = 300
    evomol_max_steps = 3
    evomol_k_to_replace = 10
    evomol_init_pop_size = 10
    evomol_n_runs = 1
    evomol_n_best_retrieved = 10
    evomol_init_pop_strategy = "random_weighted"

    # Problem action space
    max_heavy_atoms = 9
    heavy_atoms = "C,N,O,F"

    # GPR parameters (depends on the descriptor)
    gpr_alpha = 1e0

    pipeline = None

    MM_program = "rdkit"

    model = GaussianProcessRegressor(1.0*DotProduct(), alpha=gpr_alpha, optimizer=None)

    surrogate = GPRSurrogateModelWrapper(base_model=model)

    descriptor = ShinglesVectDesc(cache_location=None, count=True, vect_size=2000)

    merit = ExpectedImprovementMerit(descriptor=descriptor,
                                     pipeline=pipeline,
                                     surrogate=surrogate, init_pop_zero_EI=init_pop_zero_EI)

    objective = EvoMolEvaluationStrategyWrapper(
        OPTEvaluationStrategy(
            prop="homo",
            n_jobs=1,
            cache_files=json_cache_location_list,
            working_dir_path="/home/jleguy/dft_comput",
            MM_program=MM_program
        ),
        n_jobs=n_jobs_per_model
    )

    alg = BBOAlg(
        init_dataset_smiles=init_dataset_smiles,
        test_dataset_smiles_dict={},
        test_objectives_dict={},
        descriptor=descriptor,
        objective=objective,
        merit_function=merit,
        surrogate=surrogate,
        pipeline=pipeline,
        stop_criterion=KObjFunCallsFunctionStopCriterion(max_obj_calls),
        evomol_parameters={
            "optimization_parameters": {
                "pop_max_size": evomol_pop_max_size,
                "max_steps": evomol_max_steps,
                "k_to_replace": evomol_k_to_replace
            },
            "action_space_parameters": {
                "max_heavy_atoms": max_heavy_atoms,
                "atoms": heavy_atoms
            }
        },
        evomol_init_pop_size=evomol_init_pop_size,
        n_evomol_runs=evomol_n_runs,
        n_best_evomol_retrieved=evomol_n_best_retrieved,
        evomol_init_pop_strategy=evomol_init_pop_strategy,
        results_path=path,
        n_jobs=n_jobs_per_model,
        save_surrogate_model=save_surrogate_model,
        period_save=period_save,
        period_compute_test_predictions=period_compute_test_predictions
    )

    alg.run()

run(join(output_data_path, "0"))