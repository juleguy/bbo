import csv
import os
from os.path import join

from cache import Cache
from descriptor import ShinglesVectDesc
from evomol.evomol import run_model
from merit.expected_improvement import EIEvaluationStrategy
from objectives.similarity import ECFP4SimilarityObjective
from rdkit.Chem.rdmolfiles import MolFromSmiles, MolToSmiles
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.preprocessing import Normalizer
import numpy as np

output_prefix = "tests/1_test_optim_ei"
os.makedirs(output_prefix, exist_ok=True)

target_smi = "CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F"
troglitazone = "O=C1NC(=O)SC1Cc4ccc(OCC3(Oc2c(c(c(O)c(c2CC3)C)C)C)C)cc4"
thiothixene = "O=S(=O)(N(C)C)c2cc1C(\c3c(Sc1cc2)cccc3)=C/CCN4CCN(C)CC4"

# Reading dataset
dataset_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_all.smiles"
init_pop_smi = []
with open(dataset_path, "r") as f:
    for smi in f.readlines():
        init_pop_smi.append(smi.rstrip())

smiles_list = [MolToSmiles(MolFromSmiles(smi)) for smi in init_pop_smi[:1000]]

# smiles_list = ["C"]

dataset_smi = np.array(smiles_list)


cache = Cache()

objective = ECFP4SimilarityObjective(target_smi, cache)

# desc_builder = MBTRDesc(species=["H", "B", "C", "O", "N", "F", "Si", "P", "S", "Cl", "Se", "Br", "I"], n_jobs=12,
#                         cache=cache)


desc_builder = ShinglesVectDesc(cache=cache, count=True)
# desc_builder = MBTRDesc(cache=cache)

found_smiles = ["C"]

for i in range(5000):

    X, success = desc_builder.transform(dataset_smi)

    # Removing SMILES that caused issues when computing descriptors
    dataset_smi = dataset_smi[success]
    X = X[success]

    # Normalizer initialization
    normalizer = Normalizer()

    # Normalization
    X_train = normalizer.fit_transform(X)

    # Computing labels
    y_train = objective.fit_transform(dataset_smi)

    # Saving dataset smiles list
    smiles_list_path = join(output_prefix, "dataset" + ".smi")
    with open(smiles_list_path, "w") as f:
        # for smi in dataset_smi:
        #     f.write(smi + "\n")

        if np.sum(y_train) == 0:
            prob = np.ones(y_train.shape)
            prob = prob / np.sum(prob)
        else:
            prob = y_train / np.sum(y_train)

        drawn_smiles = np.random.choice(dataset_smi, size=10, p=prob)
        # drawn_smiles = dataset_smi[np.argsort(y_train)[::-1][:10]]

        print("init pop smiles : " + str(drawn_smiles))

        for smi in drawn_smiles:
            f.write(smi + "\n")

    # Model
    model = GaussianProcessRegressor()
    model.fit(X_train, y_train)

    X_test, _ = desc_builder.transform([target_smi, troglitazone, thiothixene])

    # Test prediction
    print("test prediction : " + str(model.predict(normalizer.transform(X_test))))

    # GuacaMol optimization
    evomol_objective = EIEvaluationStrategy(gpr_model=model, X_sample=X_train, y_sample=y_train, pipeline=normalizer,
                                            desc_builder=desc_builder)

    optimized_evomol_instance = run_model(
        {
            "obj_function": evomol_objective,
            "optimization_parameters": {
                "pop_max_size": 100,
                "k_to_replace": 10,
                "max_steps": 20,
                "mutation_max_depth": 2,
                "shuffle_init_pop": False
            },
            "action_space_parameters": {
                "max_heavy_atoms": 50
            },
            "io_parameters": {
                "model_path": join(output_prefix, "EvoMol"),
                "smiles_list_init_path": smiles_list_path
            },
        }

    )

    # Extracting optimization results
    best_solution_smi = optimized_evomol_instance.pop[
        np.argmax(optimized_evomol_instance.curr_total_scores)].to_aromatic_smiles()

    # Adding smiles to dataset
    dataset_smi = np.concatenate([dataset_smi, [best_solution_smi]])

    print("FOUND " + best_solution_smi + str(objective.fit_transform([best_solution_smi])))
    print("BEST FOUND " + str(np.max(y_train)))

    if i % 10 == 0:
        values = objective.fit_transform(dataset_smi)
        with open(join(output_prefix, "output_dataset.csv"), "w") as f:

            writer = csv.writer(f)
            for j in range(len(dataset_smi)):
                writer.writerow([dataset_smi[j], values[j]])
