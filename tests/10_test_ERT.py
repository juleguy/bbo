from evaluation import compute_ERT

print(compute_ERT([[1, 2, 4, 7], [1, 2, 3, 6]], [[2, 3, 1, 6], [3, 2, 4, 5]], targets=[1, 2, 3, 4, 5, 6, 7]))