from descriptor import MBTRDesc
from evomol.evomol.evaluation_dft import rdkit_mmff94_xyz, obabel_mmff94_xyz
import numpy as np


desc_builder_obabel = MBTRDesc(
    cache_location=None, species=["C", "H", "O", "N", "F", "S"], n_jobs=1, atomic_numbers_n=10, cosine_angles_n=25, inverse_distances_n=25,
    MM_program="obabel")


desc_builder_rdkit = MBTRDesc(
    cache_location=None, species=["C", "H", "O", "N", "F", "S"], n_jobs=1, atomic_numbers_n=10, cosine_angles_n=25, inverse_distances_n=25,
    MM_program="rdkit")

smiles = ["C", "S=CC=S"]

def compute(smi):

    print()
    print(smi)

    desc_obabel_1 = desc_builder_obabel.fit_transform([smi])[0][0]
    desc_rdkit_1 = desc_builder_rdkit.fit_transform([smi])[0][0]
    desc_obabel_2 = desc_builder_obabel.fit_transform([smi])[0][0]
    desc_rdkit_2 = desc_builder_rdkit.fit_transform([smi])[0][0]

    print("dist obabel obabel " + str(np.linalg.norm(desc_obabel_1 - desc_obabel_2)))
    print("dist rdkit rdkit " + str(np.linalg.norm(desc_rdkit_1 - desc_rdkit_2)))
    print("dist obabel rdkit " + str(np.linalg.norm(desc_obabel_1 - desc_rdkit_1)))
    print("dist obabel rdkit " + str(np.linalg.norm(desc_obabel_2 - desc_rdkit_2)))

for i in range(15):
    for smi in smiles:
        compute(smi)

print(desc_builder_obabel.fit_transform(smiles))
print(desc_builder_rdkit.fit_transform(smiles))