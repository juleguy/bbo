import os

from bboalg import BBOAlg
from descriptor import ShinglesVectDesc
from evomol.evomol import OPTEvaluationStrategy
from merit import ExpectedImprovementMerit
from model import GPRSurrogateModelWrapper
from objective import EvoMolEvaluationStrategyWrapper
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF
from sklearn.preprocessing import Normalizer
from stop_criterion import KObjFunCallsFunctionStopCriterion


def load_smiles_from_file(path, n_smiles):
    smi_list = []

    with open(path, "r") as f:
        for line in f.readlines()[:n_smiles]:
            smi_list.append(line.rstrip())

    return smi_list


json_cache_location_list = [os.environ["DATA"] + "/00_datasets/DFT/cache_OD9_step7.json",
                            os.environ["DATA"] + "/00_datasets/DFT/cache_OPT_OD9_Marta_filtered.json",
                            os.environ["DATA"] + "/00_datasets/DFT/cache_OPT_OD9_0.json",
                            os.environ["DATA"] + "/00_datasets/DFT/cache_OPT.json",
                            os.environ["DATA"] + "/00_datasets/DFT/QM9/cache_QM9.json"]

init_dataset_smiles = ["C"]

OD9_test_dataset_path = os.environ["DATA"] + "/00_datasets/DFT/OD9_7/OD9_7_smi_datasets/validation.smi"

output_data_path = "13_BBO_no_normalization"
output_data_path = "13_BBO_no_normalization_2"
output_data_path = "13_BBO_no_normalization_3"


# Parallel parameters
n_jobs_per_model = 10

# BBO parameters
test_dataset_size = 1000
save_surrogate_model = False
period_save = 1
period_compute_test_predictions = 5
max_obj_calls = 200

# EvoMol parameters
evomol_pop_max_size = 300
evomol_max_steps = 50
evomol_k_to_replace = 10
evomol_init_pop_size = 10
evomol_n_runs = 10
evomol_n_best_retrieved = 1
evomol_init_pop_strategy = "random_weighted"

# Problem action space
max_heavy_atoms = 9
heavy_atoms = "C,N,O,F"

# GPR parameters (depends on the descriptor)
gpr_alpha = 1e-1

pipeline = None

MM_program = "rdkit"

model = GaussianProcessRegressor(1.0 * RBF(1.0), alpha=gpr_alpha)

surrogate = GPRSurrogateModelWrapper(base_model=model)

descriptor = ShinglesVectDesc(cache_location=None, count=True)

merit = ExpectedImprovementMerit(descriptor=descriptor,
                                 pipeline=pipeline,
                                 surrogate=surrogate)

objective = EvoMolEvaluationStrategyWrapper(
    OPTEvaluationStrategy(
        prop="homo",
        n_jobs=1,
        cache_files=json_cache_location_list,
        working_dir_path="/home/jleguy/dft_comput",
        MM_program=MM_program
    ),
    n_jobs=n_jobs_per_model
)

OD9_7_test_dataset_smiles = load_smiles_from_file(OD9_test_dataset_path, test_dataset_size)

alg = BBOAlg(
    init_dataset_smiles=init_dataset_smiles,
    test_dataset_smiles_dict={
        "OD9_7": OD9_7_test_dataset_smiles
    },
    descriptor=descriptor,
    objective=objective,
    merit_function=merit,
    surrogate=surrogate,
    pipeline=pipeline,
    stop_criterion=KObjFunCallsFunctionStopCriterion(max_obj_calls),
    evomol_parameters={
        "optimization_parameters": {
            "pop_max_size": evomol_pop_max_size,
            "max_steps": evomol_max_steps,
            "k_to_replace": evomol_k_to_replace
        },
        "action_space_parameters": {
            "max_heavy_atoms": max_heavy_atoms,
            "atoms": heavy_atoms
        }
    },
    evomol_init_pop_size=evomol_init_pop_size,
    n_evomol_runs=evomol_n_runs,
    n_best_evomol_retrieved=evomol_n_best_retrieved,
    evomol_init_pop_strategy=evomol_init_pop_strategy,
    results_path=output_data_path,
    n_jobs=n_jobs_per_model,
    save_surrogate_model=save_surrogate_model,
    period_save=period_save,
    period_compute_test_predictions=period_compute_test_predictions

)

alg.run()