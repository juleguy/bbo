from os.path import exists

from evaluation import compute_ecdf
from postprocessing import extract_BBO_experiment_data
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set()

ECDF_TARGETS = np.arange(-10, -1, 0.01)

dot_product_no_optim_paths = ["16_BBO_DotProduct_" + str(i) for i in range(1, 9)]
dot_product_no_optim_paths_rerun = ["16_BBO_DotProduct_" + str(i) for i in range(9, 17)]
one_times_dot_product_no_optim_paths = ["17_BBO_1xDotProduct_" + str(i) for i in range(1, 9)]
one_times_dot_product_no_optim_paths_rerun = ["17_BBO_1xDotProduct_" + str(i) for i in range(9, 18)]
dot_product_optim_paths = ["19_BBO_DotProduct_hyperoptim"] + ["19_BBO_DotProduct_hyperoptim_" + str(i) for i in range(2, 11)]
one_times_dot_product_optim_paths = ["18_BBO_1xDotProduct_hyperoptim"] + ["18_BBO_1xDotProduct_hyperoptim_" + str(i) for i in range(2, 11)]


def extract_calls_obj_values_lists(paths):
    calls_lists = []
    obj_values_lists = []

    for path in paths:

        if exists(path):
            data = extract_BBO_experiment_data(path)
            calls_lists.append(data["dataset_success_n_calls"])
            obj_values_lists.append(data["dataset_success_obj_value"])

    return calls_lists, obj_values_lists


def plot(paths_list, names):
    colors = sns.color_palette()

    plt.figure(figsize=(15, 10))
    plt.xlim(0, 210)
    plt.ylim(0, 1)

    for i, paths in enumerate(paths_list):

        calls_lists, obj_values_lists = extract_calls_obj_values_lists(paths)

        for j in range(len(calls_lists)):
            calls, ecdf_vect = compute_ecdf([calls_lists[j]], [obj_values_lists[j]], targets=ECDF_TARGETS)
            # sns.lineplot(calls, ecdf_vect, dashes=[(2, 2)], style=True, color=colors[i])

        calls, ecdf_vect = compute_ecdf(calls_lists, obj_values_lists, targets=ECDF_TARGETS)

        p = sns.lineplot(calls, ecdf_vect, label=names[i], color=colors[i])
    # p.legend_.remove()
    plt.show()


plot(
    paths_list=[dot_product_no_optim_paths, dot_product_no_optim_paths_rerun, one_times_dot_product_no_optim_paths,
                one_times_dot_product_no_optim_paths_rerun, dot_product_optim_paths, one_times_dot_product_optim_paths],
    names=["DotProduct", "DotProduct_rerun", "1*DotProduct", "1*DotProduct_rerun", "DotProduct optim", "1*DotProduct optim"]
)
