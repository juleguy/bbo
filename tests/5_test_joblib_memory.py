

# Reading dataset
import os
import time

from descriptor import CoulombMatrixDesc, SOAPDesc, MBTRDesc, ShinglesVectDesc
from objectives.similarity import ECFP4SimilarityObjective
from rdkit.Chem.rdmolfiles import MolToSmiles, MolFromSmiles

dataset_path = os.environ["DATA"] + "/00_datasets/Guacamol/guacamol_v1_all.smiles"
init_pop_smi = []
with open(dataset_path, "r") as f:
    for smi in f.readlines()[:2000]:
        init_pop_smi.append(MolToSmiles(MolFromSmiles(smi.rstrip())))


cm_builder = CoulombMatrixDesc(cache_location=os.environ["CACHE_CHEM2020"], n_atoms_max=100, n_jobs=12)


print("CM")

tstart = time.time()

desc, success = cm_builder.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))

print("CM again")

tstart = time.time()

desc, success = cm_builder.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))


soap_builder = SOAPDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12)

print()
print("SOAP")

tstart = time.time()

desc, success = soap_builder.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))

print("SOAP again")

tstart = time.time()

desc, success = soap_builder.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))


mbtr_builder = MBTRDesc(cache_location=os.environ["CACHE_CHEM2020"], n_jobs=12)

print()
print("MBTR")

tstart = time.time()

desc, success = mbtr_builder.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))

print("MBTR again")

tstart = time.time()

desc, success = mbtr_builder.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))


shg_desc = ShinglesVectDesc(cache_location=os.environ["CACHE_CHEM2020"])

print()
print("Shingles vect")

tstart = time.time()

desc, success = shg_desc.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))

print("Shingles vect again")

tstart = time.time()

desc, success = shg_desc.transform(init_pop_smi)

print(len(success[success]))

print("time : " + str(time.time() - tstart))


print()
print("Celecoxib objective")

obj = ECFP4SimilarityObjective(cache_location=os.environ["CACHE_CHEM2020"],
                               target_smi="CC1=CC=C(C=C1)C2=CC(=NN2C3=CC=C(C=C3)S(=O)(=O)N)C(F)(F)F")

tstart = time.time()

y, success = obj.fit_transform(init_pop_smi)

print("time : " + str(time.time() - tstart))


print("Celecoxib objective again")

tstart = time.time()

y, success = obj.fit_transform(init_pop_smi)

print("time : " + str(time.time() - tstart))


